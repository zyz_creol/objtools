#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 22 23:17:17 2018

@author: zyzhu
"""

#%% test objtools and voxelization
from obj_utilities import read_obj, center_obj
from voxelize_obj import voxelize
import numpy as np
import matplotlib.pyplot as plt

filename='lr_shoe_pair.obj';
facet_coords=read_obj(filename);
facet_coords=center_obj(facet_coords);

facet_coords=facet_coords*0.7;

x=np.linspace(-150,150,151);
y=np.linspace(-200,200,201);
z=np.linspace(-100,100,101);
gridOUTPUT=voxelize(x,y,z,facet_coords);

(fig1, ax1) = plt.subplots(); # show a slice in the 3D volume
ax1.imshow(gridOUTPUT[:,:,50],cmap='jet', interpolation='none');
(fig2, ax2) = plt.subplots(); # show a simple projection along z direction
ax2.imshow(np.sum(gridOUTPUT,axis=2),cmap='jet', interpolation='none');