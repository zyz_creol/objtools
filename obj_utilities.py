#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Read obj file and output the coordinates of all facets
The output array takes the format of:
    dimension 1: facet id
    dimension 2: [x,y,z] coordinates of 
    dimension 3: point 1, point 2 and point 3 comprosing the facet

@author: zyzhu
"""

import pywavefront
import numpy as np

def read_obj(filename):
    scene=pywavefront.Wavefront(filename,strict=True,parse=True);
    for name,material in scene.materials.items():
        all_points=np.array(material.vertices);
        vertex_format=material.vertex_format;
    all_points_reshape=np.reshape(all_points,(-1,3),order='C')    
    if vertex_format=='N3F_V3F':
        all_points_buffer=all_points_reshape[1::2,:];
        all_points_buffer2=np.transpose(all_points_buffer);
        all_points_buffer3=np.reshape(all_points_buffer2,(3,3,-1),order='F');
        facet_coords=np.transpose(all_points_buffer3,(2,0,1));
    if vertex_format=='V3F':
        all_points_buffer=all_points_reshape;
        all_points_buffer2=np.transpose(all_points_buffer);
        all_points_buffer3=np.reshape(all_points_buffer2,(3,3,-1),order='F');
        facet_coords=np.transpose(all_points_buffer3,(2,0,1));
    return facet_coords

def center_obj(facet_coords):
    center_x=(np.amax(facet_coords[:,0,:])+np.amin(facet_coords[:,0,:]))/2;
    center_y=(np.amax(facet_coords[:,1,:])+np.amin(facet_coords[:,1,:]))/2;
    center_z=(np.amax(facet_coords[:,2,:])+np.amin(facet_coords[:,2,:]))/2;
    facet_coords_mod=facet_coords;
    facet_coords_mod[:,0,:]=facet_coords_mod[:,0,:]-center_x;
    facet_coords_mod[:,1,:]=facet_coords_mod[:,1,:]-center_y;
    facet_coords_mod[:,2,:]=facet_coords_mod[:,2,:]-center_z;
    return facet_coords_mod

def apply_matrix(facet_coords,A):
    facet_coords_mod=facet_coords;
    for loopF in range(np.shape(facet_coords)[0]):
        for loopV in range(3):
            vertex=facet_coords[loopF,:,loopV];
            vertex_mod=np.matmul(A,vertex);
            facet_coords_mod[loopF,:,loopV]=vertex_mod;
    return facet_coords_mod
    
#%% test functions in obj_utilities
#filename='12_ls_vitaminbottle1_lr.obj';
#facet_coords=read_obj(filename);
#facet_coords=center_obj(facet_coords);