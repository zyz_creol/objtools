This toolbox provides function utilities for processing .obj files. To use these functions, please copy all the files into your current work directory and import into python
The following functions are provided in each file

obj_utilities.py:
    read_obj(filename): read an .obj file
    center_obj(mesh): move the object mesh to the center of the coordinates
    apply_matrix: apply a transform matrix to all the vertices
    
voxelize_obj.py:
    voxelize(x,y,z,mesh): convert the mesh to voxels
    
These functions rely on the dependent package PyWavefront. Please instalk PyWavefront by typing in your VIRTUAL ENVIRONMENT
pip install PyWavefront.
(How to switch from default terminal to your virtual environment:
source activate MY_EVIRTUAL_NVIRONMENT_NAME)

Examples of importing these functions into python:
from obj_utilities import read_obj
from voxelize_obj import voxelize