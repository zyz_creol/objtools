#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 19 22:35:13 2018
Translated from the MATLAB function VOXELISE by Adam H. Aitkenhead,
The Christie NHS Foundation Trust. Tested for identical performance.

@author: zyzhu
"""

import numpy as np

def CONVERT_meshformat(meshXYZ):
    vertices=np.concatenate((meshXYZ[:,:,0],meshXYZ[:,:,1],meshXYZ[:,:,2]),axis=0);
    vertices=np.unique(vertices,axis=0);
    faces=np.zeros((np.shape(meshXYZ)[0],3));
    for loopF in range(np.shape(meshXYZ)[0]):
        for loopV in range(3):
            vertref=np.flatnonzero(vertices[:,0]==meshXYZ[loopF,0,loopV]);
            vertref=vertref[vertices[vertref,1]==meshXYZ[loopF,1,loopV]];
            vertref=vertref[vertices[vertref,2]==meshXYZ[loopF,2,loopV]];
            faces[loopF,loopV]=vertref;
    return faces

def COMPUTE_mesh_normals(meshXYZ):
    facetCOUNT=np.shape(meshXYZ)[0];
    coordNORMALS=np.zeros((facetCOUNT,3));
    coordVERTICES=meshXYZ;
    for loopFACE in range(facetCOUNT):
        cornerA = coordVERTICES[loopFACE,:,0];
        cornerB = coordVERTICES[loopFACE,:,1];
        cornerC = coordVERTICES[loopFACE,:,2];
        AB = cornerB-cornerA;
        AC = cornerC-cornerA;
        ABxAC = np.cross(AB,AC);
        ABxAC = ABxAC / np.linalg.norm(ABxAC);
        coordNORMALS[loopFACE,:] = ABxAC;
    return coordNORMALS

def voxelize_internal(x,y,z,facet_coords):
    
    meshXYZ=facet_coords;
    gridCOx=x;
    gridCOy=y;
    gridCOz=z;
    
    #Count the number of voxels in each direction:
    voxcountX = np.size(x);
    voxcountY = np.size(y);
    voxcountZ = np.size(z);
    
    #Prepare logical array to hold the voxelised data:
    gridOUTPUT = np.zeros((voxcountX,voxcountY,voxcountZ));
    
    #Identify the min and max x,y coordinates (cm) of the mesh:
    meshXmin = np.amin(meshXYZ[:,0,:]);
    meshXmax = np.amax(meshXYZ[:,0,:]);
    meshYmin = np.amin(meshXYZ[:,1,:]);
    meshYmax = np.amax(meshXYZ[:,1,:]);
    
    #Identify the min and max x,y coordinates (pixels) of the mesh:
    meshXminp = np.flatnonzero(np.abs(gridCOx-meshXmin)==np.amin(np.abs(gridCOx-meshXmin)))[0];
    meshXmaxp = np.flatnonzero(np.abs(gridCOx-meshXmax)==np.amin(np.abs(gridCOx-meshXmax)))[0];
    meshYminp = np.flatnonzero(np.abs(gridCOy-meshYmin)==np.amin(np.abs(gridCOy-meshYmin)))[0];
    meshYmaxp = np.flatnonzero(np.abs(gridCOy-meshYmax)==np.amin(np.abs(gridCOy-meshYmax)))[0];
    
    #Make sure min < max for the mesh coordinates:
    if meshXminp > meshXmaxp:
        (meshXminp,meshXmaxp) = (meshXmaxp,meshXminp);
    if meshYminp > meshYmaxp:
        (meshYminp,meshYmaxp) = (meshYmaxp,meshYminp);
        
    #Identify the min and max x,y,z coordinates of each facet:
    meshXYZmin = np.amin(meshXYZ,axis=2);
    meshXYZmax = np.amax(meshXYZ,axis=2);
    
    #Voxelize the mesh
    correctionLIST=np.array([[],[]]).transpose().astype('int');
    
    for loopY in range(meshYminp,meshYmaxp+1):
        possibleCROSSLISTy=np.flatnonzero(np.logical_and(meshXYZmin[:,1]<=gridCOy[loopY], meshXYZmax[:,1]>=gridCOy[loopY]));
        for loopX in range(meshXminp,meshXmaxp+1):
            possibleCROSSLIST=possibleCROSSLISTy[np.logical_and(meshXYZmin[possibleCROSSLISTy,0]<=gridCOx[loopX], meshXYZmax[possibleCROSSLISTy,0]>=gridCOx[loopX])];
            if np.size(possibleCROSSLIST)>0:
                
                facetCROSSLIST=np.array([]).astype('int');
                
                # check for crossings right on a vertex
                ind_cross=np.logical_or(\
                np.logical_or(\
                np.logical_and(meshXYZ[possibleCROSSLIST,0,0]==gridCOx[loopX], meshXYZ[possibleCROSSLIST,1,0]==gridCOy[loopY]), \
                np.logical_and(meshXYZ[possibleCROSSLIST,0,1]==gridCOx[loopX], meshXYZ[possibleCROSSLIST,1,1]==gridCOy[loopY])), \
                np.logical_and(meshXYZ[possibleCROSSLIST,0,2]==gridCOx[loopX], meshXYZ[possibleCROSSLIST,1,2]==gridCOy[loopY]));
                vertexCROSSLIST=possibleCROSSLIST[ind_cross];
                if np.size(vertexCROSSLIST)>0:
                    checkindex=np.zeros((np.size(vertexCROSSLIST)));
                    while np.amin(checkindex)==0:
                        vertexindex=np.flatnonzero(checkindex==0)[0];
                        checkindex[vertexindex]=1;
                        faces=CONVERT_meshformat(meshXYZ[vertexCROSSLIST,:,:]);
                        adjacentindex=np.isin(faces,faces[vertexindex,:]);
                        adjacentindex=np.amax(adjacentindex,axis=1);
                        checkindex[adjacentindex]=1;
                        coN=COMPUTE_mesh_normals(meshXYZ[vertexCROSSLIST[adjacentindex],:,:]);
                        if (np.amax(coN[:,2])<0) | (np.amin(coN[:,2])>0):
                            facetCROSSLIST=np.concatenate((facetCROSSLIST,np.array([vertexCROSSLIST[vertexindex]])),axis=0);
                        else:
                            possibleCROSSLIST=np.array([]);
                            correctionLIST=np.concatenate((correctionLIST,np.array([[loopX,loopY]])),axis=0);
                            checkindex[:]=1;
                
                # check for crossed facets
                if np.size(possibleCROSSLIST)>0:
                    for loopCHECKFACET in possibleCROSSLIST:
                        Y1predicted = meshXYZ[loopCHECKFACET,1,1] - ((meshXYZ[loopCHECKFACET,1,1]-meshXYZ[loopCHECKFACET,1,2]) * (meshXYZ[loopCHECKFACET,0,1]-meshXYZ[loopCHECKFACET,0,0])/(meshXYZ[loopCHECKFACET,0,1]-meshXYZ[loopCHECKFACET,0,2]));
                        YRpredicted = meshXYZ[loopCHECKFACET,1,1] - ((meshXYZ[loopCHECKFACET,1,1]-meshXYZ[loopCHECKFACET,1,2]) * (meshXYZ[loopCHECKFACET,0,1]-gridCOx[loopX])/(meshXYZ[loopCHECKFACET,0,1]-meshXYZ[loopCHECKFACET,0,2]));
                        if ((Y1predicted > meshXYZ[loopCHECKFACET,1,0]) & (YRpredicted > gridCOy[loopY])) | ((Y1predicted < meshXYZ[loopCHECKFACET,1,0]) & (YRpredicted < gridCOy[loopY])):
                            Y2predicted = meshXYZ[loopCHECKFACET,1,2] - ((meshXYZ[loopCHECKFACET,1,2]-meshXYZ[loopCHECKFACET,1,0]) * (meshXYZ[loopCHECKFACET,0,2]-meshXYZ[loopCHECKFACET,0,1])/(meshXYZ[loopCHECKFACET,0,2]-meshXYZ[loopCHECKFACET,0,0]));
                            YRpredicted = meshXYZ[loopCHECKFACET,1,2] - ((meshXYZ[loopCHECKFACET,1,2]-meshXYZ[loopCHECKFACET,1,0]) * (meshXYZ[loopCHECKFACET,0,2]-gridCOx[loopX])/(meshXYZ[loopCHECKFACET,0,2]-meshXYZ[loopCHECKFACET,0,0]));
                            if ((Y2predicted > meshXYZ[loopCHECKFACET,1,1]) & (YRpredicted > gridCOy[loopY])) | ((Y2predicted < meshXYZ[loopCHECKFACET,1,1]) & (YRpredicted < gridCOy[loopY])):
                                Y3predicted = meshXYZ[loopCHECKFACET,1,0] - ((meshXYZ[loopCHECKFACET,1,0]-meshXYZ[loopCHECKFACET,1,1]) * (meshXYZ[loopCHECKFACET,0,0]-meshXYZ[loopCHECKFACET,0,2])/(meshXYZ[loopCHECKFACET,0,0]-meshXYZ[loopCHECKFACET,0,1]));
                                YRpredicted = meshXYZ[loopCHECKFACET,1,0] - ((meshXYZ[loopCHECKFACET,1,0]-meshXYZ[loopCHECKFACET,1,1]) * (meshXYZ[loopCHECKFACET,0,0]-gridCOx[loopX])/(meshXYZ[loopCHECKFACET,0,0]-meshXYZ[loopCHECKFACET,0,1]));
                                if ((Y3predicted > meshXYZ[loopCHECKFACET,1,2]) & (YRpredicted > gridCOy[loopY])) | ((Y3predicted < meshXYZ[loopCHECKFACET,1,2]) & (YRpredicted < gridCOy[loopY])):
                                    facetCROSSLIST = np.concatenate((facetCROSSLIST,np.array([loopCHECKFACET])),axis=0);
                
                # find the z coordinate of the locations where the ray crosses each facet or vertex
                gridCOzCROSS = np.zeros(np.shape(facetCROSSLIST));
                for loopFINDZ in facetCROSSLIST:
                    planecoA = meshXYZ[loopFINDZ,1,0]*(meshXYZ[loopFINDZ,2,1]-meshXYZ[loopFINDZ,2,2]) + meshXYZ[loopFINDZ,1,1]*(meshXYZ[loopFINDZ,2,2]-meshXYZ[loopFINDZ,2,0]) + meshXYZ[loopFINDZ,1,2]*(meshXYZ[loopFINDZ,2,0]-meshXYZ[loopFINDZ,2,1]);
                    planecoB = meshXYZ[loopFINDZ,2,0]*(meshXYZ[loopFINDZ,0,1]-meshXYZ[loopFINDZ,0,2]) + meshXYZ[loopFINDZ,2,1]*(meshXYZ[loopFINDZ,0,2]-meshXYZ[loopFINDZ,0,0]) + meshXYZ[loopFINDZ,2,2]*(meshXYZ[loopFINDZ,0,0]-meshXYZ[loopFINDZ,0,1]); 
                    planecoC = meshXYZ[loopFINDZ,0,0]*(meshXYZ[loopFINDZ,1,1]-meshXYZ[loopFINDZ,1,2]) + meshXYZ[loopFINDZ,0,1]*(meshXYZ[loopFINDZ,1,2]-meshXYZ[loopFINDZ,1,0]) + meshXYZ[loopFINDZ,0,2]*(meshXYZ[loopFINDZ,1,0]-meshXYZ[loopFINDZ,1,1]);
                    planecoD = - meshXYZ[loopFINDZ,0,0]*(meshXYZ[loopFINDZ,1,1]*meshXYZ[loopFINDZ,2,2]-meshXYZ[loopFINDZ,1,2]*meshXYZ[loopFINDZ,2,1]) - meshXYZ[loopFINDZ,0,1]*(meshXYZ[loopFINDZ,1,2]*meshXYZ[loopFINDZ,2,0]-meshXYZ[loopFINDZ,1,0]*meshXYZ[loopFINDZ,2,2]) - meshXYZ[loopFINDZ,0,2]*(meshXYZ[loopFINDZ,1,0]*meshXYZ[loopFINDZ,2,1]-meshXYZ[loopFINDZ,1,1]*meshXYZ[loopFINDZ,2,0]);
                    
                    if np.abs(planecoC) < 1e-14:
                        planecoC=0;
                        
                    gridCOzCROSS[facetCROSSLIST==loopFINDZ] = (- planecoD - planecoA*gridCOx[loopX] - planecoB*gridCOy[loopY]) / planecoC;
                
                gridCOzCROSS = np.round(gridCOzCROSS*1e12)/1e12;
                gridCOzCROSS = np.unique(gridCOzCROSS);
                
                # label as inside the mesh all the voxels the ray passes through between one facet and another
                if (np.remainder(np.size(gridCOzCROSS),2)==0) & (np.size(gridCOzCROSS)>0):  # Only rays which cross an even number of facets are voxelised
                    for loopASSIGN in range(int(np.size(gridCOzCROSS)/2)):
                        voxelsINSIDE = np.logical_and(gridCOz>gridCOzCROSS[2*loopASSIGN] , gridCOz<gridCOzCROSS[2*loopASSIGN+1]);
                        gridOUTPUT[loopX,loopY,np.flatnonzero(voxelsINSIDE)] = 1;
                elif np.size(gridCOzCROSS)>0:
                    correctionLIST = np.concatenate((correctionLIST,np.array([[loopX,loopY]])),axis=0);
                    
    # interpolate the rays that cannot be voxelized
    countCORRECTIONLIST = np.shape(correctionLIST)[0];
    
    if countCORRECTIONLIST>0:
        if (np.amin(correctionLIST[:,0])==1) | (np.amax(correctionLIST[:,0])==np.size(gridCOx)) | (np.amin(correctionLIST[:,1])==1) | (np.amax(correctionLIST[:,1])==np.size(gridCOy)):
            gridOUTPUT     = np.pad(gridOUTPUT,((1,1),(1,1),(0,0)),'constant');
            correctionLIST = correctionLIST + 1;
        for loopC in range(countCORRECTIONLIST):
            voxelsforcorrection = gridOUTPUT[correctionLIST[loopC,0]-1,correctionLIST[loopC,1]-1,:]+\
            gridOUTPUT[correctionLIST[loopC,0]-1,correctionLIST[loopC,1],:]+\
            gridOUTPUT[correctionLIST[loopC,0]-1,correctionLIST[loopC,1]+1,:]+\
            gridOUTPUT[correctionLIST[loopC,0],correctionLIST[loopC,1]-1,:]+\
            gridOUTPUT[correctionLIST[loopC,0],correctionLIST[loopC,1]+1,:]+\
            gridOUTPUT[correctionLIST[loopC,0]+1,correctionLIST[loopC,1]-1,:]+\
            gridOUTPUT[correctionLIST[loopC,0]+1,correctionLIST[loopC,1],:]+\
            gridOUTPUT[correctionLIST[loopC,0]+1,correctionLIST[loopC,1]+1,:];
            
            voxelsforcorrection = np.flatnonzero(voxelsforcorrection>=4);
            gridOUTPUT[correctionLIST[loopC,0],correctionLIST[loopC,1],voxelsforcorrection] = 1;
    
      #Remove the one-pixel border surrounding the array, if this was added previously.
        if (np.shape(gridOUTPUT)[0]>np.size(gridCOx)) | (np.shape(gridOUTPUT)[1]>np.size(gridCOy)):
            gridOUTPUT = gridOUTPUT[1:-1,1:-1,:];
    
    return gridOUTPUT

def voxelize(x,y,z,meshXYZ):
    voxcountX=np.size(x);
    voxcountY=np.size(y);
    voxcountZ=np.size(z);
    gridOUTPUT=np.zeros((voxcountX,voxcountY,voxcountZ,3));
    gridOUTPUT[:,:,:,0]=np.transpose(voxelize_internal(y,z,x,meshXYZ[:,[1,2,0],:]),(2,0,1));
    gridOUTPUT[:,:,:,1]=np.transpose(voxelize_internal(z,x,y,meshXYZ[:,[2,0,1],:]),(1,2,0));
    gridOUTPUT[:,:,:,2]=voxelize_internal(x,y,z,meshXYZ);
    gridOUTPUT=np.sum(gridOUTPUT,axis=3);
    gridOUTPUT=gridOUTPUT>=1;
    return gridOUTPUT